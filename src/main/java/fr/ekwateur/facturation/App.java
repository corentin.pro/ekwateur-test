package fr.ekwateur.facturation;

import fr.ekwateur.facturation.model.Client;
import fr.ekwateur.facturation.model.IndividualClient;
import fr.ekwateur.facturation.model.Invoice;
import fr.ekwateur.facturation.model.ProfessionnalClient;

import java.math.BigDecimal;
import java.util.Optional;

public class App {

  public static void main(String[] args) throws Throwable {
    // usage expected argument :
    // args length expected : 7
    //   individual ref gender lastname firstname electricityConsumption gasConsumption

    // args length expected : 7
    //   pro        ref siret socialReason turnover electricityConsumption gasConsumption

    if (args.length != 7) {
      showErrorAndUsage();
    }

    final Client client = getClient(args).orElseThrow(App::showErrorAndUsage);


    final Invoice invoice = new Invoice(client, new BigDecimal(args[5]), new BigDecimal(args[6]));
    System.out.println("Total Electricity = " + invoice.getElectricityPrice() + " EUR");
    System.out.println("Total Gas = " + invoice.getGasPrice() + " EUR");
    System.out.println("---------------------------------");
    System.out.println("  TOTAL = " + invoice.getTotal() + " EUR");
  }

  private static <X extends Throwable> X showErrorAndUsage() {
    System.err.println("Program argument are invalid.");
    System.err.println("For individual client do as follow : ");
    System.err.println("\tindividual ref gender lastname firstname electricityConsumption gasConsumption");
    System.err.println("For professionnal client do as follow : ");
    System.err.println("\tpro        ref siret socialReason turnover electricityConsumption gasConsumption");
    throw new IllegalArgumentException("Invalid parameter");
  }

  private static Optional<Client> getClient(String[] args) {
    if ("individual".equalsIgnoreCase(args[0])) {
      return Optional.of(new IndividualClient(args[1], args[2], args[3], args[4]));
    } else if ("pro".equalsIgnoreCase(args[0])) {
      return Optional.of(new ProfessionnalClient(args[1], args[2], args[3], new BigDecimal(args[4])));
    }

    return Optional.empty();
  }
}
