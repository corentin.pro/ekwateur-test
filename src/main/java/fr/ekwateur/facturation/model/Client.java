package fr.ekwateur.facturation.model;

import java.util.regex.Pattern;

public abstract class Client {
  private static final Pattern REFERENCE_PATTERN = Pattern.compile("\\d{8}");
  private final String reference;

  public Client(String reference) {
    if (!REFERENCE_PATTERN.matcher(reference).matches()) {
      throw new IllegalArgumentException("Expecting a string containing 8 digits");
    }
    this.reference = "EKW" + reference;
  }

  public String getReference() {
    return reference;
  }


}
