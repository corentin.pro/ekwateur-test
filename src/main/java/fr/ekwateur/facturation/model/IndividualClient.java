package fr.ekwateur.facturation.model;

public class IndividualClient extends Client {

  private final String gender;
  private final String lastName;
  private final String firstName;

  public IndividualClient(String reference, String gender, String lastName, String firstName) {
    super(reference);
    this.gender = gender;
    this.lastName = lastName;
    this.firstName = firstName;
  }

  public String getGender() {
    return gender;
  }

  public String getLastName() {
    return lastName;
  }

  public String getFirstName() {
    return firstName;
  }
}
