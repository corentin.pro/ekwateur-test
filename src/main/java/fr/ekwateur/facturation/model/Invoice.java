package fr.ekwateur.facturation.model;

import fr.ekwateur.facturation.rates.RatesFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Invoice{
  private final Client client;
  private final BigDecimal electricityRate;
  private final BigDecimal gasRate;
  private final BigDecimal electricityPrice;
  private final BigDecimal gasPrice;
  private final BigDecimal total;

  public Invoice(Client client, BigDecimal electricityConsumption, BigDecimal gasConsumption) {
    this.client = client;
    electricityRate = RatesFactory.getElectricityRate(client);
    gasRate = RatesFactory.getGasRate(client);
    electricityPrice = electricityRate.multiply(electricityConsumption).setScale(2, RoundingMode.DOWN);
    gasPrice = gasRate.multiply(gasConsumption).setScale(2, RoundingMode.DOWN);
    total = electricityPrice.add(gasPrice).setScale(2, RoundingMode.DOWN);
  }

  public Client getClient() {
    return client;
  }

  public BigDecimal getElectricityRate() {
    return electricityRate;
  }

  public BigDecimal getGasRate() {
    return gasRate;
  }

  public BigDecimal getElectricityPrice() {
    return electricityPrice;
  }

  public BigDecimal getGasPrice() {
    return gasPrice;
  }

  public BigDecimal getTotal() {
    return total;
  }
}
