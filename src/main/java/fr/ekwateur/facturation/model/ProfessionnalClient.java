package fr.ekwateur.facturation.model;

import java.math.BigDecimal;

public class ProfessionnalClient extends Client {

  private final String siret;
  private final String socialReason;
  private final BigDecimal turnover;


  public ProfessionnalClient(String reference, String siret, String raisonSocial, BigDecimal turnover) {
    super(reference);
    this.siret = siret;
    this.socialReason = raisonSocial;
    this.turnover = turnover;
  }

  public String getSiret() {
    return siret;
  }

  public String getSocialReason() {
    return socialReason;
  }

  public BigDecimal getTurnover() {
    return turnover;
  }
}
