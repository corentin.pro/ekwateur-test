package fr.ekwateur.facturation.rates;

import fr.ekwateur.facturation.model.Client;
import fr.ekwateur.facturation.model.IndividualClient;
import fr.ekwateur.facturation.model.ProfessionnalClient;

import java.math.BigDecimal;

public class RatesFactory {


  public static BigDecimal getElectricityRate(Client client) {
    return client instanceof IndividualClient ?
        getElectricityRate((IndividualClient) client)
        : getElectricityRate((ProfessionnalClient) client);
  }

  public static BigDecimal getElectricityRate(IndividualClient client) {
    return new BigDecimal("0.121");
  }

  public static BigDecimal getElectricityRate(ProfessionnalClient client) {
    return client.getTurnover().compareTo(new BigDecimal(1_000_000)) > 0 ?
        new BigDecimal("0.114"):
        new BigDecimal("0.118");
  }



  public static BigDecimal getGasRate(Client client) {
    return client instanceof IndividualClient ?
        getGasRate((IndividualClient) client)
        : getGasRate((ProfessionnalClient) client);
  }

  public static BigDecimal getGasRate(IndividualClient client) {
    return new BigDecimal("0.115");
  }

  public static BigDecimal getGasRate(ProfessionnalClient client) {
    return client.getTurnover().compareTo(new BigDecimal(1_000_000)) > 0 ?
        new BigDecimal("0.111"):
        new BigDecimal("0.113");
  }


}
