package fr.ekwateur.facturation.model;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ClientTest {

  @Test
  void shouldNotAcceptInvalidReferenceNumber() {
    List.of("1111", "1234567899999999", "sdkdkkhds@")
        .forEach(ref -> {
          assertThrows(IllegalArgumentException.class,
              () -> new Client(ref) {
              });
        });
  }

  @Test
  void shouldCreateAClientGivenAValidReference() {
    // given
    final String validReference = "12345678";
    // when
    final Client client = new Client(validReference){};
    // then
    assertEquals("EKW12345678", client.getReference());

  }
}
