package fr.ekwateur.facturation.model;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class InvoiceTest {
  final Client individualClient = new IndividualClient("12345678", "M", "Banner", "Bruce");
  final Client professionnalClientSmallCA = new ProfessionnalClient("12345678", "siret", "Wayne Enterprise", new BigDecimal(100));
  final Client professionnalClientLimitCA = new ProfessionnalClient("12345678", "siret", "Wayne Enterprise", new BigDecimal(1_000_000));
  final Client professionnalClientBigCA = new ProfessionnalClient("12345678", "siret", "Wayne Enterprise", new BigDecimal(2_000_000));

  @Test
  void anInvoiceShouldHaveAIndividualClient() {
    // given
    // when
    final Invoice invoice = new Invoice(individualClient, BigDecimal.ZERO, BigDecimal.ZERO);
    // then
    assertEquals("EKW12345678", invoice.getClient().getReference());
  }

  @Test
  void anInvoiceShouldHaveAClientPro() {
    // given
    // when
    final Invoice invoice = new Invoice(professionnalClientSmallCA, BigDecimal.ZERO, BigDecimal.ZERO);
    // then
    assertEquals("EKW12345678", invoice.getClient().getReference());
  }

  @Test
  void shouldHaveFixedEnergyRateForIndividualClient() {
    // given
    final Invoice invoice = new Invoice(individualClient, BigDecimal.ZERO, BigDecimal.ZERO);
    // when
    final BigDecimal actualElectricityRate = invoice.getElectricityRate();
    final BigDecimal actualGasRate = invoice.getGasRate();
    // then
    assertEquals(new BigDecimal("0.121"), actualElectricityRate);
    assertEquals(new BigDecimal("0.115"), actualGasRate);
  }

  @Test
  void shouldHaveFixedEnergyRateForProClientWithBigCA() {
    // given
    final Invoice invoice = new Invoice(professionnalClientBigCA, BigDecimal.ZERO, BigDecimal.ZERO);
    // when
    final BigDecimal actualElectricityRate = invoice.getElectricityRate();
    final BigDecimal actualGasRate = invoice.getGasRate();
    // then
    assertEquals(new BigDecimal("0.114"), actualElectricityRate);
    assertEquals(new BigDecimal("0.111"), actualGasRate);
  }

  @Test
  void shouldHaveFixedEnergyRateForProfessionnalClientWithTurnOverBellowOrEquals1Millions() {
    // given
    List.of(
        new Invoice(professionnalClientSmallCA, BigDecimal.ZERO, BigDecimal.ZERO),
        new Invoice(professionnalClientLimitCA, BigDecimal.ZERO, BigDecimal.ZERO)
    ).forEach(invoice -> {
      // when
      final BigDecimal actualElectricityRate = invoice.getElectricityRate();
      final BigDecimal actualGasRate = invoice.getGasRate();
      // then
      assertEquals(new BigDecimal("0.118"), actualElectricityRate);
      assertEquals(new BigDecimal("0.113"), actualGasRate);
    });
  }

  @Test
  public void shouldComputeEnergyCostForIndividualClient() {
    // given
    final BigDecimal electricyConsumption = new BigDecimal(10_000);
    final BigDecimal gasConsumption = new BigDecimal(2_000);

    final Invoice invoice = new Invoice(individualClient, electricyConsumption, gasConsumption);

    // when
    final BigDecimal electricityPrice = invoice.getElectricityPrice();
    final BigDecimal gasPrice = invoice.getGasPrice();

    // then
    assertEquals(new BigDecimal("1210.00"), electricityPrice);
    assertEquals(new BigDecimal("230.00"), gasPrice);
    assertEquals(new BigDecimal("1440.00"), invoice.getTotal());
  }


  @Test
  void shouldComputeEnergyCostForProClientWithBigCA() {
    // given
    final BigDecimal electricyConsumption = new BigDecimal(10_000);
    final BigDecimal gasConsumption = new BigDecimal(2_000);

    final Invoice invoice = new Invoice(professionnalClientBigCA, electricyConsumption, gasConsumption);

    // when
    final BigDecimal electricityPrice = invoice.getElectricityPrice();
    final BigDecimal gasPrice = invoice.getGasPrice();

    // then
    assertEquals(new BigDecimal("1140.00"), electricityPrice);
    assertEquals(new BigDecimal("222.00"), gasPrice);
    assertEquals(new BigDecimal("1362.00"), invoice.getTotal());
  }

  @Test
  void shouldComputeEnergyCostForProfessionalClientWithTurnOverBellowOrEquals1Millions() {
    // given
    final BigDecimal electricyConsumption = new BigDecimal(10_000);
    final BigDecimal gasConsumption = new BigDecimal(2_000);

    List.of(
        new Invoice(professionnalClientSmallCA, electricyConsumption, gasConsumption),
        new Invoice(professionnalClientLimitCA, electricyConsumption, gasConsumption)
    ).forEach(invoice -> {

      // when
      final BigDecimal electricityPrice = invoice.getElectricityPrice();
      final BigDecimal gasPrice = invoice.getGasPrice();

      // then
      assertEquals(new BigDecimal("1180.00"), electricityPrice);
      assertEquals(new BigDecimal("226.00"), gasPrice);
      assertEquals(new BigDecimal("1406.00"), invoice.getTotal());
    });
  }


}
